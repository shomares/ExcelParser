﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FuncionesExcelExceptionTest.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ParserExcel.Tests
{
    using System;
    using System.IO;
    using ExcelParser;
    using Excepciones;
    using Fake;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Clase de prueba errors funciones  excel
    /// </summary>
    [TestClass]
    public class FuncionesExcelExceptionTest
    {
        /// <summary>
        /// Container de layouts
        /// </summary>
        private ContainerLayout container;

        /// <summary>
        /// Inicizaliza el container layout
        /// </summary>
        [TestInitialize]
        public void Initializate()
        {
            this.container = new ContainerLayout();
            this.container.RootPath = "resources/layout/";
            this.container.Layouts.TryAdd(
                "test",
                new DescriptorLayout
                {
                    Path = "test.xml"
                });

            this.container.Layouts.TryAdd(
                "notfound",
                new DescriptorLayout
                {
                    Path = "notfound.xml"
                });

            this.container.Layouts.TryAdd(
               "invalid",
               new DescriptorLayout
               {
                   Path = "invalid.xml"
               });
        }

        /// <summary>
        /// Valida la excepcion
        /// </summary>
        [TestMethod]
        public void ExceptionNotContainer()
        {
            try
            {
                var excel = new FuncionesExcel();
                var parse = excel.LoadExcel<Student>("resources/excel/LoadExcelTest.xlsx", "test");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }
        }

        /// <summary>
        /// Valida la excepcion not found exception
        /// </summary>
        [TestMethod]
        public void ExceptionFileNotFound()
        {
            try
            {
                var excel = new FuncionesExcel();
                excel.Container = this.container;
                var parse = excel.LoadExcel<Student>("resources/excel/notFound.xlsx", "test");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(FileNotFoundException));
            }
        }

        /// <summary>
        /// Valida la excepcion not found exception
        /// </summary>
        [TestMethod]
        public void ExceptionLayoutNotFound()
        {
            try
            {
                var excel = new FuncionesExcel();
                excel.Container = this.container;
                var parse = excel.LoadExcel<Student>("resources/excel/LoadExcelTest.xlsx", "notfound");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(LayoutNotFoundException));
                Assert.IsInstanceOfType(ex.InnerException, typeof(FileNotFoundException));
            }
        }

        /// <summary>
        /// Valida la excepcion not found exception
        /// </summary>
        [TestMethod]
        public void ExceptionLayoutNotFoundByKey()
        {
            try
            {
                var excel = new FuncionesExcel();
                excel.Container = this.container;
                var parse = excel.LoadExcel<Student>("resources/excel/LoadExcelTest.xlsx", "notkey");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(LayoutNotFoundException));
            }
        }

        /// <summary>
        /// Valida la excepcion not found exception
        /// </summary>
        [TestMethod]
        public void ExceptionLayoutExcel()
        {
            try
            {
                var excel = new FuncionesExcel();
                excel.Container = this.container;
                var parse = excel.LoadExcel<Student>("resources/excel/ExceptionLayoutExcel.xlsx", "test");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(HeaderMalformedException));
            }
        }

        /// <summary>
        /// Valida la excepcion invalid regex
        /// </summary>
        [TestMethod]
        public void ExceptionLayoutInvalidRegex()
        {
            try
            {
                var excel = new FuncionesExcel();
                excel.Container = this.container;
                var parse = excel.LoadExcel<Student>("resources/excel/LoadExcelTest.xlsx", "invalid");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(HeaderMalformedException));
            }
        }
    }
}
