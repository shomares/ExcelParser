﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FuncionesExcelTests.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ParserExcel.Tests
{
    using System.Linq;
    using ExcelParser;
    using Fake;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Clase de prueba de funciones excel
    /// </summary>
    [TestClass]
    public class FuncionesExcelTests
    {
        /// <summary>
        /// Container de layouts
        /// </summary>
        private ContainerLayout container;

        /// <summary>
        /// Inicizaliza el container layout
        /// </summary>
        [TestInitialize]
        public void Initializate()
        {
            this.container = new ContainerLayout();
            this.container.RootPath = "resources/layout/";
            this.container.Layouts.TryAdd(
                "test",
                new DescriptorLayout
                {
                    Path = "test.xml"
                });
        }

        /// <summary>
        /// Metodo de prueba para carga
        /// </summary>
        [TestMethod]
        public void LoadExcelTest()
        {
            var excel = new FuncionesExcel();
            excel.Container = this.container;
            var parse = excel.LoadExcel<Student>("resources/excel/LoadExcelTest.xlsx", "test");
            Assert.IsNotNull(parse.ListaObjetos);
            Assert.AreEqual(22, parse.ListaObjetos.Count());
        }

        /// <summary>
        /// Metodo de prueba para carga xls 
        /// </summary>
        [TestMethod]
        public void LoadExcelTestXls()
        {
            var excel = new FuncionesExcel();
            excel.Container = this.container;
            var parse = excel.LoadExcel<Student>("resources/excel/LoadExcelTest.xls", "test");
            Assert.IsNotNull(parse.ListaObjetos);
            Assert.AreEqual(22, parse.ListaObjetos.Count());
        }

        /// <summary>
        /// Metodo de prueba para carga xlsx con error de formato
        /// </summary>
        [TestMethod]
        public void LoadExcelTestWithFormatError()
        {
            var excel = new FuncionesExcel();
            excel.Container = this.container;
            var parse = excel.LoadExcel<Student>("resources/excel/LoadExcelTestWithFormatError.xlsx", "test");
            Assert.IsNotNull(parse.Errores);
            Assert.AreEqual(2, parse.Errores.Count());
        }

        /// <summary>
        /// Metodo de prueba para carga xls con error de formato
        /// </summary>
        [TestMethod]
        public void LoadExcelTestWithFormatErrorXLS()
        {
            var excel = new FuncionesExcel();
            excel.Container = this.container;
            var parse = excel.LoadExcel<Student>("resources/excel/LoadExcelTestWithFormatError.xls", "test");
            Assert.IsNotNull(parse.Errores);
            Assert.AreEqual(2, parse.Errores.Count());
        }

        /// <summary>
        /// Metodo de prueba para carga xlsx con error de formato
        /// </summary>
        [TestMethod]
        public void LoadExcelTestWithDataNotFound()
        {
            var excel = new FuncionesExcel();
            excel.Container = this.container;
            var parse = excel.LoadExcel<Student>("resources/excel/LoadExcelTestWithDataNotFound.xlsx", "test");
            Assert.IsNotNull(parse.Errores);
            Assert.AreEqual(4, parse.Errores.Count());
        }

        /// <summary>
        /// Metodo de prueba para carga xls con error de formato
        /// </summary>
        [TestMethod]
        public void LoadExcelTestWithDataNotFoundXLS()
        {
            var excel = new FuncionesExcel();
            excel.Container = this.container;
            var parse = excel.LoadExcel<Student>("resources/excel/LoadExcelTestWithDataNotFound.xls", "test");
            Assert.IsNotNull(parse.Errores);
            Assert.AreEqual(4, parse.Errores.Count());
        }
    }
}