﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Student.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ParserExcel.Tests.Fake
{
    using System;

    /// <summary>
    /// Clase de prueba para parsear
    /// </summary>
    public class Student
    {
        /// <summary>
        /// Propiedad entero
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Propiedad cadena de texto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Propiedad fecha
        /// </summary>
        public DateTime DateBirth { get; set; }
    }
}
