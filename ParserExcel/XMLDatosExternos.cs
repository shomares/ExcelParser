﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XMLDatosExternos.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    /// <summary>
    /// Clase que representa valores de error
    /// </summary>
    public class XMLDatosExternos
    {
        /// <summary>
        /// Crea una instancia de XMLDatosExternos
        /// </summary>
        public XMLDatosExternos()
        {
            this.LineaColumnas = 1;
            this.LeeUltimaLinea = true;
        }

        /// <summary>
        /// Indica el numero de fila de columna
        /// </summary>
        public int LineaColumnas
        {
            get; set;
        }

        /// <summary>
        /// Indica si se lee la ultima linea
        /// </summary>
        public bool LeeUltimaLinea
        {
            get; set;
        }
    }
}
