﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LayoutNotFoundException.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ParserExcel.Excepciones
{
    using System;

    /// <summary>
    /// Clase de exception para LayoutNotFound
    /// </summary>
    public class LayoutNotFoundException : Exception
    {
        /// <summary>
        /// Mensaje de error
        /// </summary>
        private const string MessageEx = "Not found file";

        /// <summary>
        /// Constructor sin argumentos
        /// </summary>
        public LayoutNotFoundException() : base()
        {
        }

        /// <summary>
        /// Constructor, con inner exception
        /// </summary>
        /// <param name="inner">Excepcion inner</param>
        public LayoutNotFoundException(Exception inner) : base(MessageEx, inner)
        {
        }
    }
}
