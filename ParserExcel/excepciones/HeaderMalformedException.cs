﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HeaderMalformedException.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ParserExcel.Excepciones
{
    using System;

    /// <summary>
    /// Excepcion que indica el error cuando el excel no cumple con el layout
    /// </summary>
    public class HeaderMalformedException : Exception
    {
        /// <summary>
        /// Constructor, para HeaderMalformedException
        /// </summary>
        public HeaderMalformedException()
        {
        }

        /// <summary>
        /// Constructor, para HeaderMalformedException
        /// </summary>
        /// <param name="field">Nombre del campo faltante</param>
        public HeaderMalformedException(string field) : base(string.Format("The field {0} no found in file", field))
        {
        }
    }
}
