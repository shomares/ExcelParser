﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListaHandler.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    using System.Collections.Generic;

    /// <summary>
    ///  Clase donde se almacena el resultado del parseo
    /// </summary>
    /// <typeparam name="T">Tipo de dato</typeparam>
    public class ListaHandler<T>
    {
        /// <summary>
        /// Lista de elementos parseados
        /// </summary>
        public IEnumerable<T> ListaObjetos { get; set; }

        /// <summary>
        /// Manejador de elementos
        /// </summary>
        public XmlHandler<T> Handler { get; set; }

        /// <summary>
        /// Lista de errores al parsear
        /// </summary>
        public IEnumerable<Errores<T>> Errores { get; set; }
    }
}
