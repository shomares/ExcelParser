﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XMLHandler.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    using System.Collections.Generic;

    /// <summary>
    /// Clase que instancia para manejar los datos de parseador
    /// </summary>
    /// <typeparam name="T">Tipo dde lista parseado</typeparam>
    public class XmlHandler<T> : XMLDatosExternos
    {
        /// <summary>
        /// Instancia para indicar como se valida la entrada del los elementos parseados
        /// </summary>
        public IValidaEntrada<T> Validador { get; set; }

        /// <summary>
        /// Instancia para indicar como se maneja la entrada de los elementos parseados
        /// </summary>
        public IManejador<T> Manejador { get; set; }

        /// <summary>
        /// Valida los elementos de la lista
        /// </summary>
        /// <param name="registros">Lista de elementos parseados</param>
        public void ValidarDatosLista(List<T> registros)
        {
            this.Validador.ValidaEntrada(registros);
        }
    }
}
