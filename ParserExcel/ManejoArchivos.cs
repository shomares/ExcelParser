﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ManejoArchivos.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    using System;

    /// <summary>
    /// Clase de extension para obtener la extension de archivo
    /// </summary>
    public static class ManejoArchivos
    {
        /// <summary>
        /// Metodo de extension para obtener la extension de archivo
        /// </summary>
        /// <param name="archivo">Path de archivo</param>
        /// <returns>Extension de archivo</returns>
        public static string ObtieneExtensionArchivo(this string archivo)
        {
            string[] arreglo;
            try
            {
                if (!string.IsNullOrEmpty(archivo))
                {
                    arreglo = archivo.Split(".".ToCharArray());
                    return arreglo[arreglo.Length - 1];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
