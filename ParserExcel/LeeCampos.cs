﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeeCampos.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;
    using ParserExcel.Excepciones;

    /// <summary>
    /// Clase estatica para validar campos
    /// </summary>
    public static class LeeCampos
    {
        /// <summary>
        /// Crea instancia de validador
        /// </summary>
        /// <typeparam name="T">Tipo a parsear</typeparam>
        /// <param name="clase">Nombre de clase</param>
        /// <param name="dato">Intancia de tipo T</param>
        /// <returns>Instancia de IValidaEntrada</returns>
        private static IValidaEntrada<T> CreaInstanciaValidador<T>(this string clase, T dato)
        {
            object aux;
            Type validaEntrada = Type.GetType(clase);
            if (validaEntrada != null)
            {
                aux = Activator.CreateInstance(validaEntrada);
                if (aux is IValidaEntrada<T>)
                {
                    return (IValidaEntrada<T>)aux;
                }
            }
            else
            {
                throw new Exception("No existe la clase: " + clase);
            }

            throw new Exception("La clase  " + clase + "no hereda de IValidaEntrada");
        }

        /// <summary>
        /// Crea instancia de manejador
        /// </summary>
        /// <typeparam name="T">Tipo de dato a parsear</typeparam>
        /// <param name="clase">Nombre de clase a instancear</param>
        /// <param name="datos">Instancia de T</param>
        /// <returns>Instancia de Manejador</returns>
        private static IManejador<T> CreaInstanciaManejador<T>(this string clase, T datos)
        {
            object aux;
            Type validaEntrada = Type.GetType(clase);
            if (validaEntrada != null)
            {
                aux = Activator.CreateInstance(validaEntrada);
                if (aux is IManejador<T>)
                {
                    return (IManejador<T>)aux;
                }
            }
            else
            {
                throw new Exception("No existe la clase: " + clase);
            }

            throw new Exception("La clase  " + clase + "no hereda de IManejador");
        }

        /// <summary>
        /// Lee el archivo layout
        /// </summary>
        /// <typeparam name="T">Tipo a parsear</typeparam>
        /// <param name="ruta">Ruta de archivo</param>
        /// <param name="datos">Intancia de T</param>
        /// <returns>Campos del layout</returns>
        public static XmlHandler<T> LeerConfiguracion<T>(this string ruta, T datos)
        {
            XmlHandler<T> campo = new XmlHandler<T>();
            XmlDocument xmld;
            XmlNodeList xmlNodeList;

            try
            {
                xmld = new XmlDocument();
                try
                {
                    xmld.Load(ruta);
                }
                catch (ArgumentException)
                {
                    xmld.LoadXml(ruta);
                }

                xmlNodeList = xmld.SelectNodes("/definicion/configuracion");
                foreach (XmlNode node in xmlNodeList)
                {
                    campo = new XmlHandler<T>();
                    foreach (XmlNode nodo in node.ChildNodes)
                    {
                        if (nodo.Name == "defcolumnas")
                        {
                            campo.LineaColumnas = int.Parse(nodo.InnerText);
                        }
                        else if (nodo.Name == "ultimaLinea")
                        {
                            campo.LeeUltimaLinea = bool.Parse(nodo.InnerText);
                        }
                        else if (nodo.Name == "validador-class")
                        {
                            IValidaEntrada<T> validador;
                            if (!string.IsNullOrEmpty(nodo.InnerText))
                            {
                                validador = CreaInstanciaValidador(nodo.InnerText, datos);
                                campo.Validador = validador;
                            }
                        }
                        else if (nodo.Name == "manejador-class")
                        {
                            IManejador<T> maneja;
                            string[] aux = new string[2];
                            aux = nodo.InnerText.Split(',');
                            if (!string.IsNullOrEmpty(nodo.InnerText))
                            {
                                maneja = CreaInstanciaManejador(nodo.InnerText, datos);
                                campo.Manejador = maneja;
                            }
                        }
                    }
                }

                return campo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lee el archivo layout
        /// </summary>
        /// <typeparam name="T">Tipo a parsear</typeparam>
        /// <param name="ruta">Ruta de archivo</param>
        /// <param name="datos">Intancia de T</param>
        /// <returns>Campos del layout</returns>
        public static XmlHandler<T> LeerConfiguracion<T>(this Stream ruta, T datos)
        {
            XmlHandler<T> campo = new XmlHandler<T>();
            XmlDocument xmld;
            XmlNodeList xmlNodeList;

            try
            {
                xmld = new XmlDocument();
                xmld.Load(ruta);

                xmlNodeList = xmld.SelectNodes("/definicion/configuracion");
                foreach (XmlNode node in xmlNodeList)
                {
                    campo = new XmlHandler<T>();
                    foreach (XmlNode nodo in node.ChildNodes)
                    {
                        if (nodo.Name == "defcolumnas")
                        {
                            campo.LineaColumnas = int.Parse(nodo.InnerText);
                        }
                        else if (nodo.Name == "ultimaLinea")
                        {
                            campo.LeeUltimaLinea = bool.Parse(nodo.InnerText);
                        }
                        else if (nodo.Name == "validador-class")
                        {
                            IValidaEntrada<T> validador;
                            validador = CreaInstanciaValidador(nodo.InnerText, datos);
                            campo.Validador = validador;
                        }
                        else if (nodo.Name == "manejador-class")
                        {
                            IManejador<T> maneja;
                            string[] aux = new string[2];
                            aux = nodo.InnerText.Split(',');
                            maneja = CreaInstanciaManejador(nodo.InnerText, datos);
                            campo.Manejador = maneja;
                        }
                    }
                }

                return campo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Obtiene la condifuracion a partir de un archivo XML
        /// </summary>
        /// <param name="ruta">Ruta del archivo xml</param>
        /// <returns>Lista de configuracion</returns>
        public static XMLDatosExternos LeerConfiguracion(this string ruta)
        {
            List<XMLDatosExternos> lista = new List<XMLDatosExternos>();
            XMLDatosExternos campo = new XMLDatosExternos();
            XmlDocument xmld;
            XmlNodeList xmlNodeList;
            try
            {
                xmld = new XmlDocument();
                try
                {
                    xmld.Load(ruta);
                }
                catch (ArgumentException)
                {
                    xmld.LoadXml(ruta);
                }

                xmlNodeList = xmld.SelectNodes("/definicion/configuracion");

                foreach (XmlNode node in xmlNodeList)
                {
                    campo = new XMLDatosExternos();
                    foreach (XmlNode nodo in node.ChildNodes)
                    {
                        if (nodo.Name == "defcolumnas")
                        {
                            campo.LineaColumnas = int.Parse(nodo.InnerText);
                        }
                        else if (nodo.Name == "ultimaLinea")
                        {
                            campo.LeeUltimaLinea = bool.Parse(nodo.InnerText);
                        }
                    }
                }

                return campo;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la condifuracion a partir de un archivo XML
        /// </summary>
        /// <param name="str">Stream de archivo</param>
        /// <returns>Lista de configuracion</returns>
        public static XMLDatosExternos LeerConfiguracion(this Stream str)
        {
            List<XMLDatosExternos> lista = new List<XMLDatosExternos>();
            XMLDatosExternos campo = new XMLDatosExternos();
            XmlDocument xmld;
            XmlNodeList xmlNodeList;
            try
            {
                xmld = new XmlDocument();
                xmld.Load(str);
                xmlNodeList = xmld.SelectNodes("/definicion/configuracion");

                foreach (XmlNode node in xmlNodeList)
                {
                    campo = new XMLDatosExternos();
                    foreach (XmlNode nodo in node.ChildNodes)
                    {
                        if (nodo.Name == "defcolumnas")
                        {
                            campo.LineaColumnas = int.Parse(nodo.InnerText);
                        }
                        else if (nodo.Name == "ultimaLinea")
                        {
                            campo.LeeUltimaLinea = bool.Parse(nodo.InnerText);
                        }
                    }
                }

                return campo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lee un estrucuta para el parseador
        /// </summary>
        /// <param name="ruta">Ruta de archivo</param>
        /// <returns>Lista de campos a parsear</returns>
        public static List<Campo> LeerEstructuraCamposXML(this string ruta)
        {
            List<Campo> lista = new List<Campo>();
            Campo campo = new Campo();
            XmlDocument xmld;
            XmlNodeList xmlNodeList;
            try
            {
                xmld = new XmlDocument();
                try
                {
                    xmld.Load(ruta);
                }
                catch (ArgumentException)
                {
                    xmld.LoadXml(ruta);
                }
                catch (FileNotFoundException ex)
                {
                    throw new LayoutNotFoundException(ex);
                }

                xmlNodeList = xmld.SelectNodes("/definicion/columnas/item");
                foreach (XmlNode node in xmlNodeList)
                {
                    campo = new Campo();
                    foreach (XmlNode nodo in node.ChildNodes)
                    {
                        if (nodo.Name == "columna")
                        {
                            campo.Nombre = nodo.InnerText;
                        }
                        else if (nodo.Name == "formato")
                        {
                            if (nodo.Attributes.Count > 0)
                            {
                                foreach (XmlNode atrib in nodo.Attributes)
                                {
                                    if (atrib.Name == "tipo")
                                    {
                                        campo.Tipo = atrib.InnerText;
                                    }
                                }
                            }

                            campo.Formato = nodo.InnerText;
                        }
                        else if (nodo.Name == "obligatorio")
                        {
                            campo.Obligatorio = nodo.InnerText;
                        }
                        else if (nodo.Name == "orden")
                        {
                            campo.Orden = int.Parse(nodo.InnerText);
                        }
                        else if (nodo.Name == "salidas")
                        {
                            campo.Salidas = new Salidas();
                            foreach (XmlNode atrib in nodo.Attributes)
                            {
                                if (atrib.Name == "opcional")
                                {
                                    campo.Salidas.Opcional = bool.Parse(atrib.InnerText);
                                }
                                else if (atrib.Name == "mapa")
                                {
                                    campo.Salidas.Mapa = atrib.InnerText;
                                }
                            }

                            foreach (XmlNode auxNodoSalida in nodo.ChildNodes)
                            {
                                if (auxNodoSalida.Name == "salida")
                                {
                                    Salida salida = new Salida();
                                    salida.Valor = auxNodoSalida.InnerText;
                                    foreach (XmlNode atrib in auxNodoSalida.Attributes)
                                    {
                                        if (atrib.Name == "opcion")
                                        {
                                            salida.Opcion = atrib.InnerText;
                                        }
                                    }

                                    campo.Salidas.Salida.Add(salida);
                                }
                            }
                        }
                        else if (nodo.Name == "valor")
                        {
                            campo.Valor.Add(nodo.InnerText);
                        }
                        else if (nodo.Name == "default")
                        {
                            campo.Defecto = nodo.InnerText;
                        }
                        else if (nodo.Name == "evalua")
                        {
                            campo.Evalua = bool.Parse(nodo.InnerText);
                        }
                    }

                    lista.Add(campo);
                }

                return lista;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Valida DataTable con los datos
        /// </summary>
        /// <typeparam name="T">Tipo de objeto parseado</typeparam>
        /// <param name="tabla">Tabla a comparar</param>
        /// <param name="campos">Campos´a validar</param>
        /// <returns>Lista de errores</returns>
        public static IEnumerable<Errores<T>> ValidaDatosDataSet<T>(this DataTable tabla, IEnumerable<Campo> campos)
        {
            string valor;
            Regex expresionrRegular;
            Match match;
            Errores<T> error;
            IList<Errores<T>> errores = new List<Errores<T>>();
            int contador = 2;
            try
            {
                if (campos.Where(X => X.Obligatorio.ToUpper() == "SI").Count() <= tabla.Columns.Count)
                {
                    foreach (DataRow row in tabla.Rows)
                    {
                        foreach (Campo campo in campos)
                        {
                            if (tabla.Columns.Contains(campo.Nombre))
                            {
                                if (!string.IsNullOrWhiteSpace(row[campo.Nombre].ToString()))
                                {
                                    if (!string.IsNullOrEmpty(campo.Formato))
                                    {
                                        expresionrRegular = new Regex(campo.Formato);

                                        if (row[campo.Nombre] != null)
                                        {
                                            if (row[campo.Nombre] is DateTime)
                                            {
                                                valor = ((DateTime)row[campo.Nombre]).ToString("dd/MM/yyyy");
                                            }
                                            else
                                            {
                                                valor = row[campo.Nombre].ToString();
                                            }

                                            match = expresionrRegular.Match(valor);
                                            if (!match.Success)
                                            {
                                                error = new Errores<T>();
                                                error.Notficacion = string.Format("The row: {0} in the field: {1} has an incorrect format", contador, campo.Nombre);
                                                error.Objeto = default(T);
                                                error.Linea = contador;
                                                errores.Add(error);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (campo.Obligatorio == "SI")
                                    {
                                        error = new Errores<T>();
                                        error.Notficacion = "The filed " + campo.Nombre + ", is required in row: " + contador;
                                        error.Objeto = default(T);
                                        error.Linea = contador;
                                        error.Property = campo.Nombre;
                                        errores.Add(error);
                                    }
                                }
                            }
                            else
                            {
                                throw new HeaderMalformedException(campo.Nombre);
                            }
                        }

                        contador++;
                    }
                }
                else
                {
                    throw new HeaderMalformedException();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return errores;
        }
    }
}
