﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IValidaEntrada.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ExcelParser
{
    using System.Collections.Generic;

    /// <summary>
    /// Clase que define que hacer con la validacion de entrada
    /// </summary>
    /// <typeparam name="T">Tipo de datos parseado</typeparam>
    public abstract class IValidaEntrada<T>
    {
        /// <summary>
        /// Instancia la lista de errores
        /// </summary>
        public IValidaEntrada()
        {
            this.Errores = new List<Errores<T>>();
        }

        /// <summary>
        /// Elementos con error
        /// </summary>
        public IEnumerable<Errores<T>> Errores { get; set; }

        /// <summary>
        /// Metodo abstracto que define la validacion de los elementos parseados
        /// </summary>
        /// <param name="registros">Elementos parseados</param>
        public abstract void ValidaEntrada(List<T> registros);
    }
}
