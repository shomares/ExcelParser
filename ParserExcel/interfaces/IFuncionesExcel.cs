﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFuncionesExcel.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    /// <summary>
    /// Interfaz que define el comportamiento del parseador
    /// </summary>
    public interface IFuncionesExcel 
    {
        /// <summary>
        /// Carga un excel y lo convierte a una lista de elementos
        /// </summary>
        /// <typeparam name="T">Tipo al que se convierte el parseador</typeparam>
        /// <param name="excel">Nombre de archivo excel</param>
        /// <param name="layout">Layout a utilizar en formato xml</param>
        /// <returns>Elementos parseados</returns>
        ListaHandler<T> LoadExcel<T>(string excel, string layout);
    }
}
