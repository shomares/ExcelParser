﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IContainerLayout.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    using System.Collections.Generic;

    /// <summary>
    /// Interfaz que define como se comporta un contenedor de layouts
    /// </summary>
    public interface IContainerLayout
    {
        /// <summary>
        /// Ruta raiz donde se encuentran los layouts
        /// </summary>
        string RootPath { get; set; }

        /// <summary>
        /// Obtiene un handler a partir del nombre 
        /// </summary>
        /// <typeparam name="T">Tipo de dato del objeto</typeparam>
        /// <param name="layout">Nombre del layout</param>
        /// <param name="objeto">Instancia del objeto</param>
        /// <returns>Lista de handler</returns>
        XmlHandler<T> GetHandler<T>(string layout, T objeto);

        /// <summary>
        /// Obtiene un layout por el nombre
        /// </summary>
        /// <param name="layout">Nombre de layouts</param>
        /// <returns>Lista de campos</returns>
        IEnumerable<Campo> GetLayout(string layout);
    }
}
