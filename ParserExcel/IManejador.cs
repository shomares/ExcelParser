﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IManejador.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ExcelParser
{
    using System.Collections.Generic;

    /// <summary>
    /// Clase abstracta que define como se maneja la lista parseada
    /// </summary>
    /// <typeparam name="T">Tipo de dato parseado</typeparam>
    public abstract class IManejador<T>
    {
        /// <summary>
        /// Lista de errores encontrados
        /// </summary>
        public List<Errores<T>> Errores { get; set; }

        /// <summary>
        /// Metodo abstracto que define que hacer con la lista parseada
        /// </summary>
        /// <param name="lista">Elementos parseados</param>
        /// <returns>Lista de elementos</returns>
        public abstract List<T> ManejameEsta(List<T> lista);
    }
}
