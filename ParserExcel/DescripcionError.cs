﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DescripcionError.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    /// <summary>
    /// Indica los los elementos a describor
    /// </summary>
    public class DescripcionError
    {
        /// <summary>
        /// Indica la descripcion del error
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Indica el numero de linea donde se encuentra el error
        /// </summary>
        public string Linea { get; set; }
    }
}
