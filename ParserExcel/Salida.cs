﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Salida.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ExcelParser
{
    /// <summary>
    /// Clase que representa valores de salida
    /// </summary>
    public class Salida
    {
        /// <summary>
        /// Indica el valor
        /// </summary>
        public string Valor { get; set; }

        /// <summary>
        /// Propiedad que indica la opcion
        /// </summary>
        public string Opcion { get; set; }
    }
}
