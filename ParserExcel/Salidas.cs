﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Salidas.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ExcelParser
{
    using System.Collections.Generic;

    /// <summary>
    /// Lista de salida
    /// </summary>
    public class Salidas
    {
        /// <summary>
        /// Contructor de elementos 
        /// </summary>
        public Salidas()
        {
            this.Salida = new List<Salida>();
        }

        /// <summary>
        /// Indica si el elemento es opcional
        /// </summary>
        public bool Opcional { get; set; }

        /// <summary>
        /// Lista de salida
        /// </summary>
        public List<Salida> Salida { get; set; }

        /// <summary>
        /// Elementos de mapa
        /// </summary>
        public string Mapa { get; set; }
    }
}