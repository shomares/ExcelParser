﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContainerLayout.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ExcelParser
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using ParserExcel.Excepciones;

    /// <summary>
    /// Implementa un contenedor de layouts
    /// </summary>
    public class ContainerLayout : IContainerLayout
    {
        /// <summary>
        /// Constructor, crea instancia de ContainerLayout
        /// </summary>
        public ContainerLayout()
        {
            this.Layouts = new ConcurrentDictionary<string, DescriptorLayout>();
        }

        /// <summary>
        ///  Bag de layouts
        /// </summary>
        public ConcurrentDictionary<string, DescriptorLayout> Layouts { get; set; }
        
        /// <summary>
        /// Tiempo en milisegundos de vida del layout
        /// </summary>
        public int TimeMax { get; set; }

        /// <summary>
        /// Ruta raiz del layout
        /// </summary>
        public string RootPath { get; set; }

        /// <summary>
        /// Obtiene una lista de campos, actualiza si es necesario
        /// </summary>
        /// <param name="layout">Lista de layouts</param>
        /// <returns>Lista de campos</returns>
        public IEnumerable<Campo> GetLayout(string layout)
        {
            DateTime now = DateTime.Now;
            if (this.Layouts.ContainsKey(layout))
            {
                var desc = this.Layouts[layout];
                TimeSpan diff = now - desc.UltimaActualizacion;
                if (desc.Campos == null || diff.Minutes > this.TimeMax)
                {
                    if (!string.IsNullOrEmpty(this.RootPath))
                    {
                        desc.Campos = LeeCampos.LeerEstructuraCamposXML(this.RootPath + desc.Path);
                    }
                    else
                    {
                        desc.Campos = LeeCampos.LeerEstructuraCamposXML(desc.Path);
                    }

                    desc.UltimaActualizacion = now;
                }

                return desc.Campos;
            }
            else
            {
                throw new LayoutNotFoundException();
            }
        }

        /// <summary>
        /// Obtiene la configuracion a partir del archivo de configuracion
        /// </summary>
        /// <typeparam name="T">Instancia de objeto</typeparam>
        /// <param name="layout">Nombre de layout</param>
        /// <param name="objeto">Instancia del objeto parseado</param>
        /// <returns>Handler de archivo parseado</returns>
        public XmlHandler<T> GetHandler<T>(string layout, T objeto)
        {
            XmlHandler<T> salida = null;
            DateTime now = DateTime.Now;
            if (this.Layouts.ContainsKey(layout))
            {
                var desc = this.Layouts[layout];
                salida = LeeCampos.LeerConfiguracion(this.RootPath + desc.Path, objeto);
            }

            return salida;
        }
    }
}
