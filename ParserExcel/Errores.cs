﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Errores.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    /// <summary>
    /// Clase que integra errores
    /// </summary>
    /// <typeparam name="T">Tipo parseado</typeparam>
    public class Errores<T>
    {
        /// <summary>
        /// Instancia con error
        /// </summary>
        public T Objeto { get; set; }

        /// <summary>
        /// Descripcion del error
        /// </summary>
        public string Notficacion { get; set; }

        /// <summary>
        /// Linea de archivo de Excel
        /// </summary>
        public int Linea { get; set; }

        /// <summary>
        /// Propiedad con el error
        /// </summary>
        public string Property { get; set; }
    }
}
