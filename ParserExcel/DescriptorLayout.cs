﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DescriptorLayout.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ExcelParser
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Describe el layout con sus propiedades
    /// </summary>
    public class DescriptorLayout
    {
        /// <summary>
        /// Ruta raiz donde se encuentran los layouts
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Lista de campos
        /// </summary>
        public IEnumerable<Campo> Campos { get; set; }

        /// <summary>
        /// Ultima fecha de actualizacion
        /// </summary>
        public DateTime UltimaActualizacion { get; set; }
    }
}
