﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Campo.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Representacion abstracta de los campos para leer el archivo excel
    /// </summary>
    public class Campo : IComparable<Campo>
    {
        /// <summary>
        /// Instancia Campo
        /// </summary>
        public Campo()
        {
            this.Valor = new List<string>();
        }

        /// <summary>
        /// Indica las salida en donde se deposita la informacion del archivo excel
        /// </summary>
        public Salidas Salidas { get; set; }

        /// <summary>
        /// Indica si es necesario realizar un compilacion en tiempo de ejecucion
        /// </summary>
        public bool Evalua { get; set; }

        /// <summary>
        /// Nombre del encabezado en el archivo excel
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Indica el elemento con que dato si no contiene dato
        /// </summary>
        public string Defecto { get; set; }

        /// <summary>
        /// Indica con que elemento se va llenar la propiedad
        /// </summary>
        public List<string> Valor { get; set; }

        /// <summary>
        /// Indica el orden de los encabezados en el archivo excel
        /// </summary>
        public int Orden { get; set; }

        /// <summary>
        /// Indica si el campo el obligatorio
        /// </summary>
        public string Obligatorio { get; set; }

        /// <summary>
        /// Indica si el campo es el formato
        /// </summary>
        public string Formato { get; set; }

        /// <summary>
        /// Indica el tipo del campo deprecated
        /// </summary>
        public string Tipo { get; set; }

        /// <summary>
        /// Implementa la interfaz IComparable
        /// </summary>
        /// <param name="other">Elemento a comparar</param>
        /// <returns>Regresa uno si es valido</returns>
        int IComparable<Campo>.CompareTo(Campo other)
        {
            if (this.Orden > other.Orden)
            {
                return 1;
            }
            else if (this.Orden < other.Orden)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
