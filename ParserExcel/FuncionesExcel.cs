﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FuncionesExcel.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ExcelParser
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using NPOI.HSSF.UserModel;
    using NPOI.SS.UserModel;
    using NPOI.XSSF.UserModel;

    /// <summary>
    /// Implementacion de FuncionesExcel con NPOI
    /// </summary>
    public class FuncionesExcel : IFuncionesExcel
    {
        /// <summary>
        /// Instancia que contiene los parseadores
        /// </summary>
        public IContainerLayout Container { get; set; }

        /// <summary>
        /// Lee el archivo de excel
        /// </summary>
        /// <param name="ruta">Path donde se encuentra el archivo de excel</param>
        /// <returns>Tabla con datos</returns>
        private DataTable GetDatosExcelXlSX(string ruta)
        {
            DataTable tabla = null;

            try
            {
                using (FileStream fs = new FileStream(ruta, FileMode.Open, FileAccess.Read))
                {
                    XSSFWorkbook hssfwb = new XSSFWorkbook(fs);
                    ISheet sheet = hssfwb.GetSheetAt(0);
                    var rw = sheet.GetRow(0);
                    var first = sheet.GetRow(1);
                    if (first != null)
                    {
                        tabla = new DataTable();
                        int columnCount = 0;
                        if (rw != null)
                        {
                            foreach (var column in rw.Cells)
                            {
                                var cell = rw.GetCell(column.ColumnIndex);
                                tabla.Columns.Add(cell.StringCellValue, typeof(object));
                                columnCount++;
                            }
                        }

                        for (int row = 1; row <= sheet.LastRowNum; row++)
                        {
                            var fila = sheet.GetRow(row);
                            DataRow dr = tabla.NewRow();
                            if (fila != null && fila.Cells != null)
                            {
                                for (int col = 0; col < columnCount; col++)
                                {
                                    var colum = fila.GetCell(col, MissingCellPolicy.RETURN_NULL_AND_BLANK);
                                    if (colum != null)
                                    {
                                        if (colum.CellType == CellType.Blank)
                                        {
                                            continue;
                                        }
                                        else if (colum.CellType == CellType.String)
                                        {
                                            dr[col] = colum.StringCellValue;
                                        }
                                        else if (colum.CellType == CellType.Numeric)
                                        {
                                            if (DateUtil.IsCellDateFormatted(colum))
                                            {
                                                dr[col] = colum.DateCellValue;
                                            }
                                            else
                                            {
                                                dr[col] = colum.NumericCellValue;
                                            }
                                        }
                                        else if (colum.CellType == CellType.Boolean)
                                        {
                                            dr[col] = colum.BooleanCellValue;
                                        }
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }

                            tabla.Rows.Add(dr);
                        }
                    }

                    if (tabla != null)
                    {
                        this.BorrarValoresNULL(ref tabla);
                    }

                    return tabla;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene datos de una rchivo de Excel 2009
        /// </summary>
        /// <param name="ruta">Ruta del archivo</param>
        /// <returns>Tabla con datos</returns>
        private DataTable GetDatosExcelXlS(string ruta)
        {
            DataTable tabla = new DataTable();

            using (FileStream fs = new FileStream(ruta, FileMode.Open, FileAccess.Read))
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook(fs);
                ISheet sheet = hssfwb.GetSheetAt(0);
                var rw = sheet.GetRow(0);
                var first = sheet.GetRow(1);
                int columnCount = 0;
                foreach (var column in rw.Cells)
                {
                    var cell = rw.GetCell(column.ColumnIndex);
                    tabla.Columns.Add(cell.StringCellValue, typeof(object));
                    columnCount++;
                }

                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    var fila = sheet.GetRow(row);
                    DataRow dr = tabla.NewRow();
                    if (fila != null && fila.Cells != null)
                    {
                        for (int col = 0; col < columnCount; col++)
                        {
                            var colum = fila.GetCell(col, MissingCellPolicy.RETURN_NULL_AND_BLANK);
                            if (colum == null || colum.CellType == CellType.Blank)
                            {
                                continue;
                            }
                            else if (colum.CellType == CellType.String)
                            {
                                dr[col] = colum.StringCellValue;
                            }
                            else if (colum.CellType == CellType.Numeric)
                            {
                                if (HSSFDateUtil.IsCellDateFormatted(colum))
                                {
                                    dr[col] = colum.DateCellValue;
                                }
                                else
                                {
                                    dr[col] = colum.NumericCellValue;
                                }
                            }
                            else if (colum.CellType == CellType.Boolean)
                            {
                                dr[col] = colum.BooleanCellValue;
                            }
                        }
                    }

                    tabla.Rows.Add(dr);
                }
            }

            this.BorrarValoresNULL(ref tabla);
            return tabla;
        }

        /// <summary>
        /// Lee datos de un archivo de excel
        /// </summary>
        /// <param name="ruta">Ruta de archivo de excel a leer</param>
        /// <param name="extension">Extension de archivo</param>
        /// <returns>Tabla con datos</returns>
        private DataTable GetDatosExcel(string ruta, string extension)
        {
            try
            {
                if (extension.Contains("xlsx"))
                {
                    return this.GetDatosExcelXlSX(ruta);
                }
                else
                {
                    return this.GetDatosExcelXlS(ruta);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Borra los valores null de la tabla
        /// </summary>
        /// <param name="tabla">Tabla a analizar</param>
        private void BorrarValoresNULL(ref DataTable tabla)
        {
            int s = 0;
            int aux = 0;
            object[] objeto;
            DataRow row;
            while (aux < tabla.Rows.Count)
            {
                s = 0;
                row = tabla.Rows[aux];
                objeto = row.ItemArray;
                if (objeto != null)
                {
                    foreach (object obj in objeto)
                    {
                        if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                        {
                            s++;
                        }

                        if (s == objeto.Length)
                        {
                            tabla.Rows.RemoveAt(aux);
                        }
                        else
                        {
                            aux++;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Parsea archivo excel
        /// </summary>
        /// <typeparam name="T">Tipo de parametro a leer</typeparam>
        /// <param name="table">Tabla con datos leidos de excel</param>
        /// <param name="campos">Lista a campos</param>
        /// <param name="aux">Instancia del parametro</param>
        /// <returns>Lista parseada del archivo de excel</returns>
        private List<T> GetDatosExcel<T>(DataTable table, IEnumerable<Campo> campos, T aux)
        {
            List<T> lista = new List<T>();
            PropertyInfo[] propiedades;
            Type tipo;
            T objeto;
            string clase;
            bool entro = false;

            try
            {
                tipo = aux.GetType();
                clase = tipo.FullName;
                propiedades = tipo.GetProperties();

                foreach (DataRow row in table.Rows)
                {
                    objeto = (T)Activator.CreateInstance(tipo);

                    foreach (Campo campo in campos)
                    {
                        entro = false;
                        foreach (string valor in campo.Valor)
                        {
                            objeto = this.GetObjeto(valor, campo, row, objeto, ref entro);
                        }
                    }

                    lista.Add(objeto);
                }

                return lista;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Parsea un campo de archivo
        /// </summary>
        /// <typeparam name="T">Tipo de parametro</typeparam>
        /// <param name="nombre">Nombre de arcjivo</param>
        /// <param name="campo">Campo a parsear</param>
        /// <param name="data">Elemento a parsear</param>
        /// <param name="aux">Tipo de archivo</param>
        /// <param name="entro">Bandera si entro</param>
        /// <returns>Objeto parseado</returns>
        private T GetObjeto<T>(string nombre, Campo campo, DataRow data, T aux, ref bool entro)
        {
            PropertyInfo[] propiedades;
            Type tipo;
            string[] campos;
            string nombrecampo;
            Type tipoPropiedad;
            int contador = 0;
            try
            {
                tipo = aux.GetType();
                propiedades = tipo.GetProperties();
                campos = nombre.Split('.');
                if (campos.Length > 0)
                {
                    nombrecampo = campos[0];
                }
                else
                {
                    nombrecampo = nombre;
                }

                foreach (PropertyInfo propiedad in propiedades)
                {
                    if (propiedad.Name == nombrecampo)
                    {
                        tipoPropiedad = propiedad.PropertyType;
                        if (tipoPropiedad == typeof(string) || tipoPropiedad.IsPrimitive || tipoPropiedad == typeof(DateTime) || tipoPropiedad == typeof(decimal))
                        {
                            contador = 0;
                            foreach (DataColumn columna in data.Table.Columns)
                            {
                                if (columna.ColumnName.Replace("#", ".") == campo.Nombre)
                                {
                                    try
                                    {
                                        propiedad.SetValue(aux, Convert.ChangeType(data[columna.ColumnName], tipoPropiedad), null);
                                        break;
                                    }
                                    catch (Exception)
                                    {
                                        throw new FormatException("El contenido de la columna " + columna.ColumnName + " no es válido");
                                    }
                                }
                                else
                                {
                                    contador++;
                                }
                            }

                            if (contador == data.Table.Columns.Count)
                            {
                                if (!string.IsNullOrEmpty(campo.Defecto))
                                {
                                    propiedad.SetValue(aux, Convert.ChangeType(campo.Defecto, tipoPropiedad), null);
                                }
                            }

                            break;
                        }
                        else if (tipoPropiedad.IsClass && !tipoPropiedad.IsGenericType)
                        {
                            string buscar = string.Empty;
                            for (int i = 1; i < campos.Length; i++)
                            {
                                buscar += campos[i];
                                if (i < campos.Length - 1)
                                {
                                    buscar += ".";
                                }
                            }

                            object objeto2;
                            Type auxc2 = propiedad.GetType();
                            objeto2 = propiedad.GetValue(aux, null);

                            if (objeto2 == null)
                            {
                                objeto2 = Activator.CreateInstance(tipoPropiedad);
                            }

                            objeto2 = this.GetObjeto(buscar, campo, data, objeto2, ref entro);
                            propiedad.SetValue(aux, objeto2, null);
                            break;
                        }
                    }
                }

                return aux;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Parsea un archivo de excel
        /// </summary>
        /// <typeparam name="T">Tipo a parsear</typeparam>
        /// <param name="excel">Ruta de archivo</param>
        /// <param name="layout">Nombre del layout a utilizar</param>
        /// <returns>Lista de objetos parseados</returns>
        public ListaHandler<T> LoadExcel<T>(string excel, string layout)
        {
            ICSharpCode.SharpZipLib.Zip.ZipConstants.DefaultCodePage = 437;
            List<T> objetos;
            IEnumerable<Campo> campos = new List<Campo>();
            DataTable tabla = null;
            XmlHandler<T> handler = null;
            ListaHandler<T> lista = new ListaHandler<T>();

            if (this.Container == null)
            {
                throw new InvalidOperationException("Not container layout");
            }

            try
            {
                T objeto = Activator.CreateInstance<T>();
                lista = new ListaHandler<T>();
                tabla = this.GetDatosExcel(excel, excel.ObtieneExtensionArchivo());
                if (tabla != null)
                {
                    campos = this.Container.GetLayout(layout);
                    handler = this.Container.GetHandler<T>(layout, objeto);
                    lista.Errores = tabla.ValidaDatosDataSet<T>(campos);
                    if (lista.Errores.Count() == 0)
                    {
                        objetos = this.GetDatosExcel(tabla, campos, objeto);

                        if (handler.Validador != null)
                        {
                            handler.Validador.Errores = lista.Errores;
                            handler.ValidarDatosLista(objetos);
                        }

                        if (handler.Manejador != null)
                        {
                            objetos = handler.Manejador.ManejameEsta(objetos);
                        }

                        lista.ListaObjetos = objetos;
                        lista.Handler = handler;
                    }
                }
                else
                {
                    throw new Exception("The file is empty");
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Wrong Local header signature"))
                {
                    throw new InvalidOperationException("File is invalid", ex);
                }

                throw ex;
            }

            return lista;
        }
    }
}
